# Terraform CI/CD Bootstrap

## Overview

This Module creates an S3 Logging Bucket and some Roles needed for the CodePipeline

## Services created

- AWS S3 Bucket
  - ACL: private
  - Encryption: AWS256

## Usage

````
module "cicd_bootstrap" {
  source  = "git@gitlab.com:tecracer-intern/terraform-landingzone/modules/terraform-aws-cicd-bootstrap.git?ref=master"

  region                              = var.region
  tf_state_bucket_arn                 = module.backend.tf_state_s3_bucket_arn
  tf_lock_table_arn                    = module.backend.tf_state_dynamodb_arn
  s3_logging_bucket_name              = "${var.organisation}-landingzone-codebuild-bucket-tflz"
  codebuild_iam_role_name             = "${var.organisation}-landingzone-codebuild-iam-role-tflz"
  codebuild_iam_role_policy_name      = "${var.organisation}-landingzone-codebuild-iam-policy-tflz"
  terraform_codecommit_repo_arn       = module.cicd_codecommit.terraform_codecommit_repo_arn
  tf_codepipeline_artifact_bucket_arn = module.cicd_codepipeline.tf_codepipeline_artifact_bucket_arn

  tags                                = local.tags
}
````

## Contributing
This module is intended to be a shared module.
Please don't commit any customer-specific configuration into this module and keep in mind that changes could affect other projects.

For new features please create a branch and submit a pull request to the maintainers.

Accepted new features will be published within a new release/tag.

## Pre-Commit Hooks

### Enabled hooks
- id: end-of-file-fixer
- id: trailing-whitespace
- id: terraform_fmt
- id: terraform_docs
- id: terraform_validate
- id: terraform_tflint

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.2.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_iam_policy.tf_iam_assumed_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_role.codebuild_iam_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role.tf_iam_assumed_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy.codebuild_iam_role_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | resource |
| [aws_iam_role_policy_attachment.tf_iam_attach_assumed_role_to_permissions_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_s3_bucket.s3_logging_bucket](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_codebuild_iam_role_name"></a> [codebuild\_iam\_role\_name](#input\_codebuild\_iam\_role\_name) | Name for IAM Role utilized by CodeBuild | `string` | n/a | yes |
| <a name="input_codebuild_iam_role_policy_name"></a> [codebuild\_iam\_role\_policy\_name](#input\_codebuild\_iam\_role\_policy\_name) | Name for IAM policy used by CodeBuild | `string` | n/a | yes |
| <a name="input_region"></a> [region](#input\_region) | Region of the Bootstrap Resources | `string` | n/a | yes |
| <a name="input_s3_logging_bucket_name"></a> [s3\_logging\_bucket\_name](#input\_s3\_logging\_bucket\_name) | Name of S3 bucket to use for access logging | `string` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | Tags that should be applied to all Resources | `any` | n/a | yes |
| <a name="input_terraform_codecommit_repo_arn"></a> [terraform\_codecommit\_repo\_arn](#input\_terraform\_codecommit\_repo\_arn) | Terraform CodeCommit git repo ARN | `string` | n/a | yes |
| <a name="input_tf_codepipeline_artifact_bucket_arn"></a> [tf\_codepipeline\_artifact\_bucket\_arn](#input\_tf\_codepipeline\_artifact\_bucket\_arn) | Codepipeline artifact bucket ARN | `string` | n/a | yes |
| <a name="input_tf_lock_table_arn"></a> [tf\_lock\_table\_arn](#input\_tf\_lock\_table\_arn) | ARN of the Terraform State S3 Bucket | `string` | n/a | yes |
| <a name="input_tf_state_bucket_arn"></a> [tf\_state\_bucket\_arn](#input\_tf\_state\_bucket\_arn) | ARN of the Terraform State S3 Bucket | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_codebuild_iam_role_arn"></a> [codebuild\_iam\_role\_arn](#output\_codebuild\_iam\_role\_arn) | Output the CodeBuild IAM role |
| <a name="output_s3_logging_bucket"></a> [s3\_logging\_bucket](#output\_s3\_logging\_bucket) | n/a |
| <a name="output_s3_logging_bucket_id"></a> [s3\_logging\_bucket\_id](#output\_s3\_logging\_bucket\_id) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
